//élément qui varie - compteur
//Au démarrage elle vaut 0
//Elle n'est exécutée qu'une fois lors de la première lecture du script
let count = 0; 

//on déclare nos fonctions - exécutées que si on les appelle
//appeler une fonction : soustraction() ou avec un écouteur d'évenement
function changeCouleur(){
    if (count<0){
        document.getElementById("resultat").style.color="red";
    } else if(count>0){
        document.getElementById("resultat").style.color="green";
    } else {
        document.getElementById("resultat").style.color="black";
    }
}

function soustraction(){
    count = count - 1;
    changeCouleur();
    document.getElementById("resultat").innerHTML = count;
}

function reset(){
    count = 0;
    changeCouleur();
    document.getElementById("resultat").innerHTML = count;
}

function addition(){
    count = count+1;
    changeCouleur();
    document.getElementById("resultat").innerHTML = count;
}

//écouteur d'élément
//première partie (objet+fonction) cible l'élément - la seconde (propriétés) permet si il y a un click alors on appele la fonction
document.getElementById("decrease").addEventListener("click", soustraction); 
document.getElementById("increase").addEventListener("click", addition);
document.getElementById("reset").addEventListener("click", reset);